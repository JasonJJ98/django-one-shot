from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm


# Create your views here.
def showTodoList(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def showTodoItem(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/detail.html", context)


def CreateTodo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def UpdateTodo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoForm(instance=todo)

    context = {
        "form": form,
        "todo_object": todo,
    }

    return render(request, "todos/update.html", context)


def DeleteTodo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/delete.html", context)


def CreateItem(request):
    # items = TodoList.objects.all()
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()

    context = {
        "form": form,
        # "items": items,
    }

    return render(request, "todos/itemcreate.html", context)


def UpdateItem(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm(instance=item)

    context = {
        "form": form,
        "item": item,
    }

    return render(request, "todos/itemupdate.html", context)
