from todos.views import showTodoList, showTodoItem, CreateTodo, UpdateTodo
from todos.views import DeleteTodo, CreateItem, UpdateItem
from django.urls import path

urlpatterns = [
    path("", showTodoList, name="todo_list_list"),
    path("<int:id>/", showTodoItem, name="todo_list_detail"),
    path("create/", CreateTodo, name="todo_list_create"),
    path("<int:id>/edit/", UpdateTodo, name="todo_list_update"),
    path("<int:id>/delete/", DeleteTodo, name="todo_list_delete"),
    path("items/create/", CreateItem, name="todo_item_create"),
    path("items/<int:id>/edit/", UpdateItem, name="todo_item_update")
]
